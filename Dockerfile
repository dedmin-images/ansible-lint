FROM python:3-slim

LABEL maintainer="https://gitlab.com/dedmin"

RUN adduser  ansibler
USER ansibler
WORKDIR /home/ansibler

ENV PATH="/home/ansibler/.local/bin:${PATH}"

RUN pip3 install --user rich==10.16.2 ansible==7.7.0 ansible-lint==4.3.7